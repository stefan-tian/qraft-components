import styled from 'styled-components'
import { Tabs as BPTabs, Tab as BPTab } from '@blueprintjs/core'

export const Tabs = styled(BPTabs)`
  && {
    display: flex;
    flex-direction: column;
    .bp3-tab-list {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-flex: 0;
      -ms-flex: 0 0 auto;
      flex: 0 0 auto;
      -webkit-box-align: end;
      -ms-flex-align: end;
      align-items: flex-end;
      position: relative;
      margin: 0;
      border: none;
      padding: 0;
      list-style: none;
      border-bottom: 1px solid #ececec;
      margin-bottom: 16px;
      .bp3-tab-indicator-wrapper {
        width: initial !important;
      }
      .bp3-tab {
        font-size: 18px;
        color: #585858;
        line-height: 1.78;
        letter-spacing: 0.9px;
        margin-right: 20px;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        word-wrap: normal;
        -webkit-box-flex: 0;
        -ms-flex: 0 0 auto;
        flex: 0 0 auto;
        position: relative;
        cursor: pointer;
        max-width: 100%;
        vertical-align: top;
        &[aria-disabled='true'] {
          color: #585858;
          font-size: 18px;
          font-weight: normal;
        }
        &[aria-selected='true'] {
          color: #585858;
          font-size: 18px;
          font-weight: bold;
          -webkit-box-shadow: inset 0 -3px 0 #ff730e !important;
          box-shadow: inset 0 -3px 0 #ff730e !important;
        }
      }
    }
    .bp3-tab-panel {
      display: flex;
      -webkit-box-orient: horizontal;
      -webkit-box-direction: normal;
      -ms-flex-direction: row;
      flex-direction: row;
      /* margin: auto; */
      margin-top: 0;
      &[aria-hidden='true'] {
        display: none;
      }
    }
  }
`

export const Tab = BPTab
