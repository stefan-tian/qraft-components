import styled from 'styled-components'
import { RadioGroup as BPRadioGroup } from '@blueprintjs/core'

const RadioGroup = styled(BPRadioGroup)`
  .bp3-control.bp3-radio .bp3-control-indicator {
    display: none;
  }

  .bp3-control.bp3-radio {
    display: flex;
    &:not(:last-child) {
      margin-bottom: 14px;
    }

    input[type='radio'] {
      /* display: none; */
      margin-right: 12px;
      opacity: 1;
    }

    input[type='radio']:checked:before {
      width: 18px;
      height: 18px;
      border-radius: 50%;
      top: -3px;
      left: -3px;
      position: relative;
      background-color: #fff;
      content: '';
      display: inline-block;
      visibility: visible;
      border: 2px solid #ff730e;
    }

    input[type='radio']:before {
      width: 18px;
      height: 18px;
      border-radius: 50%;
      top: -3px;
      left: -3px;
      position: relative;
      background-color: #fff;
      content: '';
      display: inline-block;
      visibility: visible;
      border: 2px solid #9b9b9b;
    }

    input[type='radio']:checked:after {
      width: 9.6px;
      height: 9.6px;
      border-radius: 50%;
      top: -20px;
      left: 1.4px;
      position: relative;
      background-color: #ff730e;
      content: '';
      display: inline-block;
      visibility: visible;
    }
  }
`

export default RadioGroup
