import React from 'react'
import styled, { css } from 'styled-components'

const StyledTag = styled.span`
  display: inline-block;
  font-size: 12px;
  letter-spacing: 0.58px;
  font-weight: 500;
  padding: 2px 6px;
  line-height: 18px;
  border-radius: 4px;
  font-weight: 500;
  margin-right: ${({ mr }) => mr};
  margin-bottom: ${({ mb }) => mb};

  ${({ ongoing }) =>
    ongoing &&
    css`
      background-color: #ff730e;
      color: white;
    `}

  ${({ finished }) =>
    finished &&
    css`
      background-color: #ececec;
      color: #9b9b9b;
    `}
`

const Tag = ({ text, finished, ongoing, mr, mb }) => (
  <StyledTag finished={finished} ongoing={ongoing} mr={mr} mb={mb}>
    {text}
  </StyledTag>
)

export default Tag
