import React from 'react'
import styled from 'styled-components'

const Container = styled.div`
  height: 18px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  cursor: pointer;
`

const Bar = styled.span`
  display: inline-block;
  background-color: #9b9b9b;
  height: 2px;
  width: 24px;
`

const Hamburger = () => (
  <Container>
    <Bar />
    <Bar />
    <Bar />
  </Container>
)

export default Hamburger
