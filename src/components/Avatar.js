import styled from 'styled-components'

export default styled.div`
  display: inline-block;
  vertical-align: middle;
  width: ${({ w }) => (w ? w : '40px')};
  height: ${({ w }) => (w ? w : '40px')};
  margin-right: ${({ mr }) => mr};
  margin-bottom: ${({ mb }) => mb};
  border-radius: 50%;
  overflow: hidden;
  background-color: #f8f8f8;
  box-shadow: 0 5px 10px 0px rgba(0, 0, 0, 0.1);
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  cursor: pointer;
  background-image: ${props => `url(${props.src ? props.src : '/avatar.svg'})`};
`
