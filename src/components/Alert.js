import React from 'react'
import { Alert as BPAlert } from '@blueprintjs/core'
import styled from 'styled-components'
import Text from './Text'
import Icon from './Icon'

const StyledAlert = styled(BPAlert)`
  padding: 20px 20px 12px 24px;
  display: flex;
  width: 100%;
  position: relative;

  .bp3-button {
    font-size: 16px;
    line-height: 24px;
    letter-spacing: 0.2px;
    padding: 8px 20px;
    font-weight: 500;
  }

  .bp3-button:nth-child(1) {
    color: #fff;
    background: #ff730e;
    border: 0;

    &:hover {
      background: #ff8e1e;
    }
  }
`

const Close = styled.div`
  position: absolute;
  right: 20px;
  top: 20px;
  cursor: pointer;
`

const Alert = ({ path }) => (
  <StyledAlert isOpen={true} cancelButtonText="返回" confirmButtonText="確定">
    <Text lg mb="14px" bold>
      確定要離開嗎？
    </Text>
    <Close>
      <Icon path={path ? path : '/icon-close.svg'} w="24px" />
    </Close>
    <Text md ls="0.2px" mb="60px">
      離開後將不會儲存你所更改的項目
    </Text>
  </StyledAlert>
)

export default Alert
