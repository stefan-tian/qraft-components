import React from 'react'
import styled from 'styled-components'
import { Popover } from '@blueprintjs/core'
import Text from './Text'
import Icon from './Icon'

const Content = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 336px;
`

const TitleContainer = styled.div`
  padding: 12px 12px 8px 12px;
`

const Sepline = styled.span`
  display: inline-block;
  height: 1px;
  opacity: 0.3;
  width: 312px;
  background-color: #979797;
`

const Insight = styled.div`
  display: flex;
  align-items: center;
  padding: 12px;
  padding-right: 14px;
`

const IdContainer = styled.div`
  min-width: 20px;
  margin-right: 9px;
  font-size: 16px;
  letter-spacing: 0.2px;
  line-height: 24px;
  color: #ff730e;
  font-weight: 500;
`

const StyledText = styled(Text)`
  cursor: pointer;
  &:hover {
    color: #ff730e;
  }
`

const IconContainer = styled.div`
  min-width: 36px;
  display: flex;
  align-items: center;
  justify-content: flex-end;
`

export const PopupList = ({ children }) => (
  <Content>
    <TitleContainer>
      <Text md bold grayDarker>
        觀點列表
      </Text>
      <Sepline />
    </TitleContainer>
    {children}
  </Content>
)

export const PopupListItem = ({
  insight,
  canListen,
  index,
  onClickInsight
}) => {
  index += 1
  const displayId = index < 10 ? `0${index}` : index
  return (
    <Insight onClick={onClickInsight}>
      <IdContainer>{displayId}</IdContainer>
      <StyledText md grayDarker mr="auto">
        {insight}
      </StyledText>
      <IconContainer>
        {canListen ? null : <Icon w="20px" path="/lock.svg" />}
      </IconContainer>
    </Insight>
  )
}
