import React from 'react'
import styled from 'styled-components'

export const Header = styled.header`
  z-index: 10;
  display: flex;
  align-items: center;
  margin: 0;
  background-color: #fff;
  box-shadow: 0 5px 10px 0px rgba(0, 0, 0, 0.1);
  position: sticky;
  top: 0px;
  padding: 12px 16px;
`

export const Logo = styled.span`
  display: inline-block;
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  height: 38px;
  width: 102px;
  margin-right: auto;
  background-image: url('/logo-light.svg');
`

const RightHead = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`

export const LgScRightHead = styled(RightHead)`
  @media (max-width: 768px) {
    display: none;
  }
`

export const SmScRightHead = styled(RightHead)`
  display: none;

  @media (max-width: 768px) {
    display: flex;
  }
`
