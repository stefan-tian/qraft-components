import React from 'react'
import { Toaster as BPToaster, Position } from '@blueprintjs/core'
import styled from 'styled-components'
import Icon from './Icon'

export const ToastIconContainer = styled.div`
  position: relative;
  top: 10px;
  left: 20px;
  margin-right: 18px;
`

const ToasterInstance =
  typeof window !== 'undefined'
    ? BPToaster.create({
        position: Position.TOP
      })
    : null

const Toaster = ({ text, timeout, iconWidth, iconPath }) =>
  ToasterInstance.show({
    message: text,
    timeout,
    icon: (
      <ToastIconContainer>
        <Icon w={iconWidth} path={iconPath} />
      </ToastIconContainer>
    )
  })

export default Toaster
