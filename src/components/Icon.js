import React from 'react'
import styled, { css } from 'styled-components'

const StyledIcon = styled.div`
  display: inline-block;
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  background-image: ${({ path }) => `url(${path})`};
  width: ${({ w }) => (w ? w : '24px')};
  height: ${({ w }) => (w ? w : '24px')};
  margin-right: ${({ mr }) => mr};
  margin-bottom: ${({ mb }) => mb};
  ${({ pointer }) =>
    pointer &&
    css`
      cursor: pointer;
    `}

  ${({ inverse }) =>
    inverse &&
    css`
      transform: rotate(180deg);
    `}
`

const Icon = ({ path, w, mr, mb, pointer, inverse, onClick }) => (
  <StyledIcon
    path={path}
    w={w}
    mr={mr}
    mb={mb}
    pointer={pointer}
    inverse={inverse}
    onClick={onClick}
  />
)

export default Icon
