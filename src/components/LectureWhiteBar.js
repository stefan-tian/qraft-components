import React from 'react'
import styled from 'styled-components'
import { Collapse } from '@blueprintjs/core'
import Text from './Text'
import { IconTextBtn } from './TextBtn'
import Tag from './Tag'
import Icon from './Icon'

const WhiteBar = styled.div`
  margin: 0 auto;
  max-width: 940px;
  padding: 24px;
  box-shadow: 0 2px 17px 0 rgba(0, 0, 0, 0.1);
  margin-right: ${({ mr }) => mr};
  margin-bottom: ${({ mb }) => mb};
`

const WhiteBarContainer = styled.div`
  display: flex;
  align-items: center;
`

const LeftBar = styled.div`
  display: flex;
  align-items: center;
  margin-right: auto;
`

const RightBar = styled.div`
  display: flex;
  align-items: center;

  @media (max-width: 768px) {
    display: none;
  }
`

const BottomBar = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`

const RightBarInfo = styled.div`
  display: flex;
  align-items: center;
  margin-right: 12px;

  @media (max-width: 768px) {
    margin-top: 12px;
    margin-bottom: 12px;
  }
`
const RightBarButton = styled.div`
  display: flex;
  align-items: center;
`

const CollapseContent = styled.div`
  display: none;

  @media (max-width: 768px) {
    display: flex;
  }
`

const Sepline = styled.span`
  display: inline-block;
  width: 1px;
  height: 20px;
  background-color: #ececec;
  margin-right: 12px;
`

class LectureWhiteBar extends React.Component {
  state = {
    isOpen: false
  }

  handleClick = () => {
    this.setState(prev => ({ isOpen: !prev.isOpen }))
  }

  render() {
    const {
      mr,
      mb,
      status,
      startDate,
      endDate,
      followed,
      joined,
      onAddToCalendar,
      onShare
    } = this.props
    return (
      <WhiteBar mr={mr} mb={mb}>
        <WhiteBarContainer>
          <LeftBar>
            {status === 'ongoing' ? (
              <Tag ongoing text="進行中" mr="8px" />
            ) : status === 'finished' ? (
              <Tag finished text="已完成" mr="8px" />
            ) : null}
            <Text md grayDarker bold ls="0.2px">
              {startDate} - {endDate}
            </Text>
          </LeftBar>
          <RightBar>
            <RightBarInfo>
              <Text gray md mr="12px">
                {followed} 關注
              </Text>
              <Text gray md mr="16px">
                {joined} 參與
              </Text>
              <Sepline />
            </RightBarInfo>
            <RightBarButton>
              <IconTextBtn
                text="加入行事曆"
                path="/date.svg"
                space="4px"
                iconWidth="20px"
                onClick={onAddToCalendar}
                mr="12px"
              />
              <Sepline />
              <IconTextBtn
                text="分享"
                path="/share2.svg"
                space="4px"
                iconWidth="20px"
                onClick={onShare}
              />
            </RightBarButton>
          </RightBar>
          <CollapseContent>
            {this.state.isOpen ? (
              <Icon
                onClick={this.handleClick}
                inverse
                pointer
                path="/icon-arrow.svg"
                w="32px"
              />
            ) : (
              <Icon
                onClick={this.handleClick}
                pointer
                path="/icon-arrow.svg"
                w="32px"
              />
            )}
          </CollapseContent>
        </WhiteBarContainer>
        <Collapse isOpen={this.state.isOpen}>
          <BottomBar>
            <RightBarInfo>
              <Text gray md mr="12px">
                {followed} 關注
              </Text>
              <Text gray md>
                {joined} 參與
              </Text>
            </RightBarInfo>
            <RightBarButton>
              <IconTextBtn
                text="加入行事曆"
                path="/date.svg"
                space="4px"
                iconWidth="20px"
                onClick={onAddToCalendar}
                mr="12px"
              />
              <Sepline />
              <IconTextBtn
                text="分享"
                path="/share2.svg"
                space="4px"
                iconWidth="20px"
                onClick={onShare}
              />
            </RightBarButton>
          </BottomBar>
        </Collapse>
      </WhiteBar>
    )
  }
}

export default LectureWhiteBar
