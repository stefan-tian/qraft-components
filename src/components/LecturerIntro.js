import React from 'react'
import styled from 'styled-components'
import Avatar from './Avatar'
import Text from './Text'

const LecturerContainer = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 19px;
`

const Container = styled.div`
  max-width: 300px;
  margin-bottom: ${({ mb }) => mb};
  margin-right: ${({ mr }) => mr};
`

const LecturerIntro = ({ name, title, intro, mr, mb, avatarUrl }) => (
  <Container mr={mr} mb={mb}>
    <LecturerContainer>
      <Avatar mr="12px" url={avatarUrl} w="56px" />
      <div>
        <Text grayDarker lg lh="27px">
          {name}
        </Text>
        <Text gray sm>
          {title}
        </Text>
      </div>
    </LecturerContainer>
    <Text justify grayDarker md lh="27px">
      {intro}
    </Text>
  </Container>
)

export default LecturerIntro
