import React from 'react'
import styled from 'styled-components'
import Icon from './Icon'

const Button = styled.button`
  background-color: #ffe1cb;
  width: 44px;
  height: 44px;
  border: 0;
  border-radius: 4px;
  cursor: pointer;
  margin-right: ${({ mr }) => mr};
  margin-bottom: ${({ mb }) => mb};

  &:hover {
    background-color: rgba(255, 225, 201, 0.8);
  }
`

const Inside = styled.span`
  display: flex;
  flex-direction: column;
  align-items: center;
  font-size: 12px;
  color: #ff730e;
  line-height: 18px;
  letter-spacing: 0.58px;
`

export const IconBtn = props => (
  <Button mr={props.mr} mb={props.mb} onClick={props.onClick}>
    <Inside>
      <Icon path={props.path} w={props.iconWidth} />
      {props.text}
    </Inside>
  </Button>
)
