import React from 'react'
import styled from 'styled-components'
import Avatar from './Avatar'
import Text from './Text'

const Container = styled.div`
  background-color: #f7f8f8;
  max-width: 320px;
  padding: 24px;
  display: flex;
  flex-direction: column;
  margin-bottom: ${({ mb }) => mb};
  margin-right: ${({ mr }) => mr};
`

const Lecturer = styled.div`
  display: flex;
  align-items: center;
`

const LecturePaymentInfo = ({
  startDate,
  endDate,
  title,
  avatarUrl,
  name,
  mb,
  mr
}) => (
  <Container mb={mb} mr={mr}>
    <Text md grayDarker mb="12px">
      {startDate} - {endDate}
    </Text>
    <Text xl grayDarker bold mb="24px">
      {title}
    </Text>
    <Lecturer>
      <Avatar w="40px" url={avatarUrl} mr="10px" />
      <Text gray md>
        {name}
      </Text>
    </Lecturer>
  </Container>
)

export default LecturePaymentInfo
