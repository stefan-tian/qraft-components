import styled, { css } from 'styled-components'

const Text = styled.h5`
  font-weight: 500;
  margin: 0;
  color: #585858;
  margin-right: ${({ mr }) => mr};
  margin-bottom: ${({ mb }) => mb};
  font-size: ${({ fs }) => fs};

  ${({ justify }) =>
    justify &&
    css`
      text-align: justify;
    `} 
    
  ${({ center }) =>
    center &&
    css`
      text-align: center;
    `} 

  ${({ regular }) =>
    regular &&
    css`
      font-weight: normal;
    `}

  ${({ bold }) =>
    bold &&
    css`
      font-weight: bold;
    `}

  ${({ main }) =>
    main &&
    css`
      color: #ff730e;
    `}

  ${({ white }) =>
    white &&
    css`
      color: #fff;
    `}

  ${({ gray }) =>
    gray &&
    css`
      color: #9b9b9b;
    `}

  ${({ alert }) =>
    alert &&
    css`
      color: #ed4b34;
    `}

  ${({ grayLight }) =>
    grayLight &&
    css`
      color: #cdcdcd;
    `}

  ${({ grayDark }) =>
    grayDark &&
    css`
      color: #7b7b7b;
    `}
  
  ${({ xs }) =>
    xs &&
    css`
      font-size: 12px;
      letter-spacing: 0.58px;
    `}

  ${({ sm }) =>
    sm &&
    css`
      font-size: 14px;
      letter-spacing: 0.67px;
      line-height: 18px;
    `}

  ${({ md }) =>
    md &&
    css`
      font-size: 16px;
      letter-spacing: 0.77px;
      line-height: 24px;
    `}

  ${({ lg }) =>
    lg &&
    css`
      font-size: 18px;
      letter-spacing: 0.86px;
    `}

  ${({ xl }) =>
    xl &&
    css`
      font-size: 20px;
      letter-spacing: 0.77px;
      line-height: 26px;
      font-weight: bold;
    `}

  letter-spacing: ${({ ls }) => ls};
  line-height: ${({ lh }) => lh};
`

export default Text
