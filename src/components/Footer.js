import styled from 'styled-components'

export const Footer = styled.div`
  position: sticky;
  z-index: 10;
  bottom: 0px;
  background-color: #fff;
  box-shadow: 0 2px 10px 0 rgba(0, 0, 0, 0.1);
  padding: 12px 16px;
`

export const FooterContainer = styled.div`
  margin: 0 auto;
  max-width: 940px;
  display: flex;
  align-items: center;

  @media (max-width: 576px) {
    flex-direction: column;
    align-items: flex-start;
  }
`

export const LeftFooter = styled.div`
  display: flex;
  align-items: center;
  margin-right: auto;

  @media (max-width: 576px) {
    margin-bottom: 8px;
  }
`

export const RightFooter = styled.div`
  display: flex;
  align-items: center;

  @media (max-width: 576px) {
    width: 100%;
  }
`
