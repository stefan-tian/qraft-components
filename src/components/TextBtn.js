import React from 'react'
import styled from 'styled-components'
import Icon from './Icon'

const Flex = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;
  margin-right: ${({ mr }) => mr};
  margin-bottom: ${({ mb }) => mb};
`

const StyledTextBtn = styled.span`
  display: inline-block;
  font-size: ${({ fs }) => (fs ? fs : '16px')};
  letter-spacing: 0.2px;
  font-weight: 500;
  /* Maybe change colors to props */
  color: ${({ color }) => (color ? color : '#ff730e')};
  line-height: 24px;
  margin-left: ${({ ml }) => ml};
  cursor: pointer;
  margin-right: ${({ mr }) => mr};
  margin-bottom: ${({ mb }) => mb};
`

export const TextBtn = ({ mr, mb, ml, color, text, fs, onClick }) => (
  <StyledTextBtn
    mr={mr}
    mb={mb}
    ml={ml}
    color={color}
    fs={fs}
    onClick={onClick}
  >
    {text}
  </StyledTextBtn>
)

export const IconTextBtn = ({
  mr,
  mb,
  path,
  iconWidth,
  space,
  text,
  fs,
  onClick
}) => (
  <Flex mr={mr} mb={mb} onClick={onClick}>
    <Icon path={path} w={iconWidth} />
    <TextBtn ml={space} text={text} fs={fs} />
  </Flex>
)
