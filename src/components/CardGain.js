import React from 'react'
import styled from 'styled-components'
import Text from './Text'
import Icon from './Icon'

const Container = styled.div`
  display: flex;
  align-items: center;
  min-width: 300px;
  max-width: 300px;
  flex-direction: column;
  padding: 16px 28px 28px 19px;
  margin-right: ${({ mr }) => mr};
  margin-bottom: ${({ mb }) => mb};
`

const CardGain = ({ path, title, stage, message, mr, mb }) => (
  <Container mr={mr} mb={mb}>
    <Icon w="72px" path={path} mb="8px" />
    <Text lg ls="0.8px" lh="27px">
      {title}
    </Text>
    <Text sm ls="0.8px" lh="20px" mb="12px">
      {stage}
    </Text>
    <Text md center ls="0.2px">
      {message}
    </Text>
  </Container>
)

export default CardGain
