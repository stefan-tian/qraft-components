import React from 'react'
import { Popover as BPPopver } from '@blueprintjs/core'
import styled from 'styled-components'
import Icon from './Icon'
import Text from './Text'

const Content = styled.div`
  padding: 17px 16px;
  max-width: 260px;
  display: flex;
  flex-direction: column;
`

const Clickable = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;
  margin-right: ${({ mr }) => mr};
  margin-bottom: ${({ mb }) => mb};
`

const Flex = styled.div`
  display: flex;
  align-items: center;
`
const PopContent = () => (
  <Content>
    <Flex>
      <Icon path="/check-main.svg" w="14px" mr="4px" mb="2px" />
      <Text sm ls="0.4px" gray mb="2px">
        保證回覆
      </Text>
    </Flex>
    <Text sm ls="0.4px" gray mb="12px">
      講者於「討論互動期」內保證回覆你所提出的問題，歡迎踴躍參與互動～
    </Text>
    <Flex>
      <Icon path="/check-main.svg" w="14px" mr="4px" mb="2px" />
      <Text sm ls="0.4px" gray mb="2px">
        保存重聽
      </Text>
    </Flex>
    <Text sm ls="0.4px" gray>
      Qraft將保存你所參與的內容，讓你隨時瀏覽重聽。
    </Text>
  </Content>
)

const PopTarget = ({ mr, mb }) => (
  <Clickable mb={mb} mr={mr}>
    <Icon w="14px" path="/check-main.svg" mr="4px" />
    <Text md mr="4px">
      保證回覆
    </Text>
    <Icon w="14px" path="/check-main.svg" mr="4px" />
    <Text md mr="4px">
      保存重聽
    </Text>
  </Clickable>
)

const Popover = ({ mr, mb }) => (
  <BPPopver content={<PopContent />} target={<PopTarget mr={mr} mb={mb} />} />
)

export default Popover
