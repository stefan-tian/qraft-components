import React from 'react'
import styled from 'styled-components'
import Avatar from './Avatar'
import Text from './Text'

export const DropDownContainer = styled.div`
  max-width: 260px;
  /* min-width: 260px; */
  background-color: #fff;

  @media (max-width: 380px) {
    max-width: 375px;
  }
`

export const DropDownMobileInfo = styled.div`
  display: none;

  @media (max-width: 380px) {
    display: flex;
    flex-direction: column;
  }
`

const UserContainer = styled.div`
  display: flex;
  align-items: center;
  padding: 24px 20px;
  background-color: #f7f8f8;
`

export const DropDownItem = styled.div`
  cursor: pointer;
  padding: 14px 20px 20px 16px;
  font-size: 16px;
  line-height: 24px;
  letter-spacing: 0.2px;
  font-weight: 500;
  color: #585858;
  &:hover {
    color: #ff730e;
    background-color: #fff;
  }
`

export const DropDownSepline = styled.span`
  display: inline-block;
  height: 1px;
  padding-left: 3px;
  padding-right: 3px;
  background-color: #ececec;
  margin-bottom: 16px;
  margin-top: 8px;
`

export const DropDownUserInfo = ({ avatarUrl, name }) => (
  <UserContainer>
    <Avatar mr="24px" src={avatarUrl} />
    <div>
      <Text grayDarker lg>
        {name}
      </Text>
      <Text gray md>
        歡迎回來
      </Text>
    </div>
  </UserContainer>
)
