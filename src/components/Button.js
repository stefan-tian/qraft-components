import React from 'react'
import styled, { css } from 'styled-components'

const StyledButton = styled.button`
  font-size: ${({ fs }) => (fs ? fs : '16px')};
  line-height: 24px;
  border-radius: 4px;
  font-weight: 500;
  margin-right: ${({ mr }) => mr};
  margin-bottom: ${({ mb }) => mb};

  cursor: pointer;
  ${({ primary }) =>
    primary &&
    css`
      background-color: #fff;
      color: #585858;
      border: 1px solid #cdcdcd;
      padding: 9px 19px;
      letter-spacing: 0.2px;

      &:hover {
        color: #ff730e;
        border-color: #ff730e;
      }

      &:active {
        color: #ec6d11;
        border-color: #ec6d11;
      }

      &:disabled {
        color: #cdcdcd;
        border-color: #ececec;
        cursor: not-allowed;
      }
    `}

  ${({ main }) =>
    main &&
    css`
      background-color: #ff730e;
      color: #fff;
      letter-spacing: 0.77px;
      padding: 10px 56px;
      border: 0;

      &:hover {
        background-color: #ff8e1e;
      }

      &:active {
        background-color: #ec6d11;
      }

      &:disabled {
        background-color: #ececec;
        cursor: not-allowed;
      }
    `}

    ${({ stretchy }) =>
      stretchy &&
      css`
        @media (max-width: 576px) {
          width: calc(100% - (44px + 12px)) !important;
        }
      `}

  padding: ${({ px, py }) => `${px} ${py}`};  
`

export const Button = ({
  primary,
  main,
  fs,
  mr,
  mb,
  text,
  stretchy,
  onClick
}) => (
  <StyledButton
    primary={primary}
    main={main}
    fs={fs}
    mr={mr}
    mb={mb}
    stretchy={stretchy}
    onClick={onClick}
  >
    {text}
  </StyledButton>
)
