import React from 'react'
import styled from 'styled-components'
import Text from './Text'
import { IconTextBtn } from './TextBtn'

const InsightContainer = styled.div`
  max-width: 780px;
  display: flex;
  align-items: center;
  background-color: #f7f8f8;
  cursor: pointer;
  margin-right: ${({ mr }) => mr};
  margin-bottom: ${({ mb }) => mb};
`

const InsightIdContainer = styled.div`
  padding: 16px 13px;
  min-width: 82px;
`

const InsightContentContainer = styled.div`
  display: flex;
  border-left: 1px solid #ececec;
  padding: 16px;
  align-items: center;
`

const ListenContainer = styled.div`
  min-width: 50px;
  margin-left: 20px;
`

const LectureInsight = ({
  idx,
  text,
  onClickInsight,
  onClickListen,
  canListen,
  mb,
  mr
}) => {
  idx += 1
  if (idx < 10) {
    idx = `0${idx}`
  }
  return (
    <InsightContainer onClick={onClickInsight} mr={mr} mb={mb}>
      <InsightIdContainer>
        <Text sm gray ls="0.4px">
          觀點 {idx}
        </Text>
      </InsightIdContainer>
      <InsightContentContainer>
        {text}
        {canListen ? (
          <ListenContainer>
            <IconTextBtn
              text="試聽"
              path="/icon-headset.svg"
              iconWidth="16px"
              space="4px"
              fs="14px"
              onClick={onClickListen}
            />
          </ListenContainer>
        ) : null}
      </InsightContentContainer>
    </InsightContainer>
  )
}

export default LectureInsight
