import React from 'react'
import styled from 'styled-components'

const Td = styled.td`
  border: 1px solid black;
`

const TableComponent = ({ propDefinitions }) => {
  const props = propDefinitions.map(({ property, propType, description }) => (
    <tr key={property}>
      <Td>{property}</Td>
      <Td>{propType}</Td>
      <Td>{description}</Td>
    </tr>
  ))

  return (
    <table style={{ border: '1px solid #000000', borderCollapse: 'collapse' }}>
      <thead>
        <tr>
          <th>name</th>
          <th>type</th>
          <th>description</th>
        </tr>
      </thead>
      <tbody>{props}</tbody>
    </table>
  )
}

export default TableComponent
