import { configure, addDecorator } from '@storybook/react'
import { withNotes } from '@storybook/addon-notes'
import { withInfo } from '@storybook/addon-info'

addDecorator(withInfo)
addDecorator(withNotes)
function loadStories() {
  require('../stories/index.js')
}

configure(loadStories, module)
