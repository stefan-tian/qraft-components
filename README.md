# qraft-components

- a reusable component library for qraft

## Getting Started

- To see some examples of the components, run `yarn storybook`

## Issues

- custom .babelrc file would break storybook, so there are now two branches.

## Branches

- master

  - can run `yarn storybook` without problems to see existing components

- packaging-latest

  - consists of files and scripts needed for publishing to npm
