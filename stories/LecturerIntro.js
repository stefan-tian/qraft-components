import React from 'react'
import { storiesOf } from '@storybook/react'
import { LecturerIntro } from '../src/components'
import TableComponent from '../examples/TableComponent'

const propDefinitions = [
  {
    property: 'name',
    propType: 'String',
    description: 'The name of the lecturer'
  },
  {
    property: 'avatarUrl',
    propType: 'String',
    description: 'The url of the avatar photo. e.g. /avatar.svg'
  },
  {
    property: 'title',
    propType: 'String',
    description: 'The title of the lecturer'
  },
  {
    property: 'intro',
    propType: 'String',
    description: 'The introduction of the lecturer'
  },
  {
    property: 'mr',
    propType: 'String',
    description: 'customize margin right. e.g. "5px"'
  },
  {
    property: 'mb',
    propType: 'String',
    description: 'customize margin bottom. e.g. "5px"'
  }
]

storiesOf('LecturerIntro', module).add(
  'LecturerIntro',
  () => (
    <LecturerIntro
      name="蘇婷"
      title="法國游擊者"
      intro="我曾經在不懂程式、不懂群眾募資的情況下90天與夥伴從無到有設計了一款程式教育桌遊，並且募資超過百萬，也曾經在不懂社群經營的情況下，1個月內粉絲人數破萬，最高單月265萬人次瀏覽，還出版了一本書，在博客來科學新書銷售排行榜第一，甚至沒打過工，不懂調酒，在100天內開了一間酒吧。 "
    />
  ),
  {
    info: {
      inline: true,
      TableComponent: () => <TableComponent propDefinitions={propDefinitions} />
    }
  }
)
