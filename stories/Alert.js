import React from 'react'
import { storiesOf } from '@storybook/react'
import { Alert } from '../src/components'
import TableComponent from '../examples/TableComponent'

const propDefinitions = [
  {
    property: 'path',
    propType: 'String',
    description: 'The path to the X icon'
  }
]

storiesOf('Alert', module).add('alert', () => <Alert />, {
  info: {
    inline: true,
    TableComponent: () => <TableComponent propDefinitions={propDefinitions} />
  }
})
