import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import { Popover } from '@blueprintjs/core'
import { PopupList, PopupListItem, Button } from '../src/components'
import TableComponent from '../examples/TableComponent'

const propDefinitions = [
  {
    property: 'insight',
    propType: 'String',
    description: 'The text of the insight'
  },
  {
    property: 'canListem',
    propType: 'Boolean',
    description: 'Can the user listen without paying'
  },
  {
    property: 'index',
    propType: 'Number',
    description:
      'The index of the insight(starts from 0). e.g. if you put in 0, the display would be 0 + 1'
  },
  {
    property: 'onClickInsight',
    propType: 'Function',
    description: 'Handler when clicking the insight'
  }
]

const insightList = [
  {
    insight: '為什麼說現在是入門 UI 設計黃金階段設計黃金階段？',
    canListen: true
  },
  {
    insight: '換上設計師的眼睛深度思考介面',
    canListen: true
  },
  {
    insight: '為什麼說現在是入門 UI 設計',
    canListen: false
  },
  {
    insight:
      '為什麼說現在是入門為什麼說現在是入門為什麼說現在是入門為什麼說現在是入門 UI 設計',
    canListen: false
  }
]

storiesOf('InsightListPopover', module)
  .addParameters({
    info: {
      inline: true
    }
  })
  .add(
    'PopupListItem',
    () => (
      <PopupListItem
        insight="換上設計師的眼睛深度思考介面"
        canListen={false}
        index={1}
        onClickInsight={action('You clicked the insight')}
      />
    ),
    {
      info: {
        TableComponent: () => (
          <TableComponent propDefinitions={propDefinitions} />
        )
      }
    }
  )
  .add('PopupList', () => (
    <PopupList>
      {insightList.map(({ insight, canListen }, index) => (
        <PopupListItem
          key={insight}
          insight={insight}
          canListen={canListen}
          index={index}
          onClickInsight={action('You clicked the insight')}
        />
      ))}
    </PopupList>
  ))
  .add('InsightListPopoverExample', () => (
    <Popover
      content={
        <PopupList>
          {insightList.map(({ insight, canListen }, index) => (
            <PopupListItem
              key={insight}
              insight={insight}
              canListen={canListen}
              index={index}
              onClickInsight={action('You clicked the insight')}
            />
          ))}
        </PopupList>
      }
      target={<Button text="CLICK ME" />}
    />
  ))
