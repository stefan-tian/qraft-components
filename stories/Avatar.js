import React from 'react'
import { storiesOf } from '@storybook/react'
import { Avatar } from '../src/components'
import TableComponent from '../examples/TableComponent'

Avatar.displayName = 'Avatar'

const propDefinitions = [
  {
    property: 'mb',
    propType: 'String',
    description: 'Customize margin bottom. e.g. "30px"'
  },
  {
    property: 'mr',
    propType: 'String',
    description: 'Customize margin right. e.g. "15px"'
  },
  {
    property: 'w',
    propType: 'String',
    description:
      'Customize the width of the avatar, default is 40px. e.g. "35px"'
  },
  {
    property: 'url',
    propType: 'String',
    description: 'Set the image url, default is "/avatar.svg"'
  }
]

storiesOf('Avatar', module).add('Q匠', () => <Avatar />, {
  notes:
    'component: Avatar\nprops: w: avatar width and height, src: image url, mr/mb: margin-right/bottom',
  info: {
    inline: true,
    TableComponent: () => <TableComponent propDefinitions={propDefinitions} />
  }
})
