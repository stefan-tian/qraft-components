import React from 'react'
import { storiesOf } from '@storybook/react'
import {
  Header,
  Logo,
  SmScRightHead,
  LgScRightHead,
  TextBtn,
  Avatar,
  Hamburger,
  Icon,
  Button
} from '../src/components'

Header.displayName = 'Header'
Logo.displayName = 'Logo'
LgScRightHead.displayName = 'LgScRightHead'
SmScRightHead.displayName = 'SmScRightHead'

storiesOf('Header', module)
  .addParameters({
    info: {
      inline: true
    }
  })
  .add('not-authenticated', () => (
    <Header>
      <Logo />
      <Button primary text="登入/註冊" />
    </Header>
  ))
  .add('authenticated', () => (
    <Header>
      <Logo />
      <span>
        <LgScRightHead>
          <TextBtn color="#585858" mr="32px" text="探索講座" />
          <TextBtn color="#585858" mr="32px" text="成為講者" />
          <TextBtn color="#585858" mr="35px" text="我的講座" />
          <Icon pointer w="24px" path="/remind.svg" mr="32px" />
          <Avatar w="32px" />
        </LgScRightHead>
        <SmScRightHead>
          <Icon pointer w="24px" path="/remind.svg" mr="32px" />
          <Hamburger />
        </SmScRightHead>
      </span>
    </Header>
  ))
