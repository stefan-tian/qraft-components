import React from 'react'
import { storiesOf } from '@storybook/react'
import { LectureInsight } from '../src/components'
import TableComponent from '../examples/TableComponent'

const propDefinitions = [
  {
    property: 'idx',
    propType: 'String',
    description:
      'The id you want to display plus 1. e.g. if you put in 0, the display would be one'
  },
  {
    property: 'onClickInsight',
    propType: 'Function',
    description: 'Event handler when you click the insigh'
  },
  {
    property: 'onClickListen',
    propType: 'Function',
    description: 'Event handler when you click 試聽 button'
  },
  {
    property: 'text',
    propType: 'String',
    description: 'The content of the insight'
  },
  {
    property: 'canListen',
    propType: 'Boolean',
    description: 'Show listen button when true, hide when false'
  },
  {
    property: 'mr',
    propType: 'String',
    description: 'customize margin right. e.g. "5px"'
  },
  {
    property: 'mb',
    propType: 'String',
    description: 'customize margin bottom. e.g. "5px"'
  }
]

storiesOf('LectureInsight', module)
  .addParameters({
    info: {
      inline: true,
      TableComponent: () => <TableComponent propDefinitions={propDefinitions} />
    }
  })
  .add('can listen', () => (
    <LectureInsight
      idx={0}
      onClickInsight={() => console.log('You clicked the insight')}
      onClickListen={() => console.log("I'm listening")}
      text="關於這堂課名字很長的時候關於這堂課名字很長的時候關於這堂課名字很長的時候關於這堂課名字很長的時候"
      canListen={true}
    />
  ))
  .add('cannot listen', () => (
    <LectureInsight
      idx={1}
      onClickInsight={() => console.log('You clicked the insight')}
      onClickListen={() => console.log("I'm listening")}
      text="關於這堂課名字很長的時候關於這堂課名字很長的時候關於這堂課名字很長的時候關於這堂課名字很長的時候"
      canListen={false}
    />
  ))
