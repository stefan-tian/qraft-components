import React from 'react'
import { storiesOf } from '@storybook/react'
import { IconBtn } from '../src/components'
import TableComponent from '../examples/TableComponent'

const propDefinitions = [
  {
    property: 'text',
    propType: 'String',
    description: 'The text of the icon + text button'
  },
  {
    property: 'mb',
    propType: 'String',
    description: 'Customize margin bottom. e.g. "30px"'
  },
  {
    property: 'mr',
    propType: 'String',
    description: 'Customize margin right. e.g. "15px"'
  },
  {
    property: 'path',
    propType: 'String',
    description: 'The url of the icon'
  },
  {
    property: 'iconWidth',
    propType: 'String',
    description: "The width of the icon, default is 24px. e.g. '18px'"
  },
  {
    property: 'onClick',
    propType: 'Function',
    description: 'handle click event"'
  }
]

storiesOf('IconBtns', module).add(
  'listen',
  () => <IconBtn text="試聽" path="/icon-headset.svg" iconWidth="18px" />,
  {
    notes:
      'component: IconBtn\nprops: text: text in the button, icon: icon name, iconWidth: the width of icon, mr/mb: margin-right/bottom',
    info: {
      inline: true,
      TableComponent: () => <TableComponent propDefinitions={propDefinitions} />
    }
  }
)
