import React from 'react'
import { storiesOf } from '@storybook/react'
import { LectureWhiteBar } from '../src/components'
import TableComponent from '../examples/TableComponent'

const propDefinitions = [
  {
    property: 'status',
    propType: 'String',
    description:
      'ongoing will show 進行中 tag, finished will show 已完成 tag, other strings will not show any tag'
  },
  {
    property: 'startDate',
    propType: 'String',
    description: 'The date the lecture starts'
  },
  {
    property: 'endDate',
    propType: 'String',
    description: 'The date the lecture ends'
  },
  {
    property: 'followed',
    propType: 'Number',
    description: 'The number of people following this lecture'
  },
  {
    property: 'joined',
    propType: 'Number',
    description: 'The number of people who has joined the lecture'
  },
  {
    property: 'mr',
    propType: 'String',
    description: 'customize margin right. e.g. "5px"'
  },
  {
    property: 'mb',
    propType: 'String',
    description: 'customize margin bottom. e.g. "5px"'
  },
  {
    property: 'onShare',
    propType: 'Function',
    description: 'handle event when click 分享'
  },
  {
    property: 'onAddToCalendar',
    propType: 'Function',
    description: 'handle event when click 加入行事曆"'
  }
]

storiesOf('LectureWhiteBar', module)
  .addParameters({
    info: {
      inline: true,
      TableComponent: () => <TableComponent propDefinitions={propDefinitions} />
    }
  })
  .add('ongoing', () => (
    <LectureWhiteBar
      status="ongoing"
      startDate="2018/11/10(日)"
      endDate="11/20(二)"
      followed={3566}
      joined={188}
      onAddToCalendar={() => console.log('added to calendar')}
      onShare={() => console.log('copied link')}
    />
  ))
  .add('finished', () => (
    <LectureWhiteBar
      status="finished"
      startDate="2018/11/10(日)"
      endDate="11/20(二)"
      followed={3566}
      joined={188}
      onAddToCalendar={() => console.log('added to calendar')}
      onShare={() => console.log('copied link')}
    />
  ))
  .add('no-status', () => (
    <LectureWhiteBar
      startDate="2018/11/10(日)"
      endDate="11/20(二)"
      followed={3566}
      joined={188}
      onAddToCalendar={() => console.log('added to calendar')}
      onShare={() => console.log('copied link')}
    />
  ))
