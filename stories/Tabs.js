import React from 'react'
import { storiesOf } from '@storybook/react'
import { Tabs, Tab } from '../src/components'

Tabs.displayName = 'Tabs'
Tab.displayName = 'Tab'
storiesOf('Tabs', module).add(
  'Tabs',
  () => (
    <Tabs id="dissAndRate">
      <Tab id="discussion" title="活動前問答" />
      <Tab id="rating" title="活動評價" />
    </Tabs>
  ),
  {
    info: {
      inline: true
    },
    notes:
      'Refer to blueprint for more details: https://blueprintjs.com/docs/#core/components/tabs'
  }
)
