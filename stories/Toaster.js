import React from 'react'
import { storiesOf } from '@storybook/react'
import { Toaster } from '../src/components'
import TableComponent from '../examples/TableComponent'

const propDefinitions = [
  {
    property: 'text',
    propType: 'String',
    description: 'The text message of the toaster'
  },
  {
    property: 'timout',
    propType: 'Number',
    description:
      'Milliseconds before the toaster vanishes. If set to value less then 0, the toaster will stay until the cross has been clicked. e.g. 1000, or -1'
  },
  {
    property: 'iconWidth',
    propType: 'String',
    description: 'The width of the icon. e.g. "20px"'
  },
  {
    property: 'iconPath',
    propType: 'String',
    description: "The path of the icon. e.g. '/check.svg'"
  }
]

storiesOf('Toaster', module).add(
  'toaster',
  () => (
    <Toaster
      text="已複製分享連結"
      timeout={-1}
      iconWidth="20px"
      iconPath="/icons-checked.svg"
    />
  ),
  {
    info: {
      inline: true,
      TableComponent: () => <TableComponent propDefinitions={propDefinitions} />
    }
  }
)
