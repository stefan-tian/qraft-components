import React from 'react'
import { storiesOf } from '@storybook/react'
import { CardGain } from '../src/components'
import TableComponent from '../examples/TableComponent'

const propDefinitions = [
  {
    property: 'title',
    propType: 'String',
    description: 'The title of the card'
  },
  {
    property: 'stage',
    propType: 'String',
    description: 'The stage of the card: 講座前、講座中、講座後'
  },
  {
    property: 'message',
    propType: 'String',
    description: 'The message of the card'
  },
  {
    property: 'path',
    propType: 'String',
    description: 'The path of the icon'
  },
  {
    property: 'mr',
    propType: 'String',
    description: 'customize margin right. e.g. "5px"'
  },
  {
    property: 'mb',
    propType: 'String',
    description: 'customize margin bottom. e.g. "5px"'
  }
]

storiesOf('CardGain', module).add(
  'CardGain',
  () => (
    <CardGain
      title="講者親自解惑"
      stage="講座前"
      path="/icon-lecture-gain-01.svg"
      message="任何人都可參與活動前問答，講師將會根據大家的問題調整主題方向"
    />
  ),
  {
    info: {
      inline: true,
      TableComponent: () => <TableComponent propDefinitions={propDefinitions} />
    }
  }
)
