import React from 'react'
import { storiesOf } from '@storybook/react'
import { LecturePaymentInfo } from '../src/components'
import TableComponent from '../examples/TableComponent'

const propDefinitions = [
  {
    property: 'name',
    propType: 'String',
    description: 'The name of the lecturer'
  },
  {
    property: 'avatarUrl',
    propType: 'String',
    description: 'The url of the avatar photo. e.g. /avatar.svg'
  },
  {
    property: 'title',
    propType: 'String',
    description: 'The title of the lecture'
  },
  {
    property: 'startDate',
    propType: 'String',
    description: 'The date the lecture starts'
  },
  {
    property: 'endDate',
    propType: 'String',
    description: 'The date the lecture ends'
  },
  {
    property: 'mr',
    propType: 'String',
    description: 'customize margin right. e.g. "5px"'
  },
  {
    property: 'mb',
    propType: 'String',
    description: 'customize margin bottom. e.g. "5px"'
  }
]

storiesOf('LecturePaymentInfo', module).add(
  'LecturePaymentInfo',
  () => (
    <LecturePaymentInfo
      name="王大明"
      title="怎樣擁有從名稱名稱名稱名稱名稱名稱"
      startDate="2018/11/10(日)"
      endDate="11/20(二)"
    />
  ),
  {
    info: {
      inline: true,
      TableComponent: () => <TableComponent propDefinitions={propDefinitions} />
    }
  }
)
