import React from 'react'
import { storiesOf } from '@storybook/react'
import { RadioGroup } from '../src/components'

const options = [
  {
    label: '選項一',
    value: 1
  },
  {
    label: '選項二',
    value: 2
  },
  {
    label: '選項三',
    value: 3
  }
]

RadioGroup.displayName = 'RadioGroup'

storiesOf('RadioGroup', module).add(
  'radio-optoins',
  () => (
    <RadioGroup
      label="請選擇"
      name="demo"
      onChange={() => console.log('clicked')}
      options={options}
      selectedValue={2}
    />
  ),
  {
    notes:
      'component: RadioGroup, it is built on top of blurprint RadioGroup, see https://blueprintjs.com/docs/#core/components/radio for props details',
    info: {
      inline: true
    }
  }
)
