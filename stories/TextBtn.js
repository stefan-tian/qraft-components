import React from 'react'
import { storiesOf } from '@storybook/react'
import { TextBtn, IconTextBtn } from '../src/components'
import TableComponent from '../examples/TableComponent'

const propDefinitions = [
  {
    property: 'text',
    propType: 'String',
    description: 'The text of the text button'
  },
  {
    property: 'mb',
    propType: 'String',
    description: 'Customize margin bottom. e.g. "30px"'
  },
  {
    property: 'mr',
    propType: 'String',
    description: 'Customize margin right. e.g. "15px"'
  },
  {
    property: 'color',
    propType: 'String',
    description: 'Customize the text color. e.g. "#fff"'
  },
  {
    property: 'onClick',
    propType: 'Function',
    description: 'Handle onClick'
  }
]

const propDefinitionsIcon = [
  ...propDefinitions,
  {
    property: 'path',
    propType: 'String',
    description: 'The url of the icon'
  },
  {
    property: 'space',
    propType: 'String',
    description: "The space between icon and text. e.g. '30px'"
  },
  {
    property: 'iconWidth',
    propType: 'String',
    description: "The width of the icon, the default is 24px. e.g. '30px'"
  },
  {
    property: 'onClick',
    propType: 'Function',
    description: 'handle click event"'
  }
]

storiesOf('TextBtns', module)
  .addParameters({
    info: {
      inline: true
    }
  })
  .add('no-icon', () => <TextBtn text="回覆" />, {
    notes: 'component: TextBtn',
    info: {
      TableComponent: () => <TableComponent propDefinitions={propDefinitions} />
    }
  })
  .add(
    'with-icon',
    () => (
      <IconTextBtn
        text="分享"
        path="/share2.svg"
        space="4px"
        iconWidth="20px"
      />
    ),
    {
      notes:
        'component: IconTextBtn\nprops: icon: icon name, iconWidth: the width of icon, space: margin-left of the text(space between icon and text',
      info: {
        TableComponent: () => (
          <TableComponent propDefinitions={propDefinitionsIcon} />
        )
      }
    }
  )
