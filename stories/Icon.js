import React from 'react'
import { storiesOf } from '@storybook/react'
import { Icon } from '../src/components'
import TableComponent from '../examples/TableComponent'

const propDefinitions = [
  {
    property: 'icon',
    propType: 'String',
    description: 'the name of the icon. e.g. date'
  },
  {
    property: 'w',
    propType: 'String',
    description: 'customize the width, default is 24px. e.g. "30px"'
  },
  {
    property: 'pointer',
    propType: 'Boolean',
    description: 'Turn cursor to pointer'
  },
  {
    property: 'mr',
    propType: 'String',
    description: 'customize margin right. e.g. "5px"'
  },
  {
    property: 'mb',
    propType: 'String',
    description: 'customize margin bottom. e.g. "5px"'
  },
  {
    property: 'inverse',
    propType: 'Boolean',
    description: 'If true, rotate the icon by 180 deg, see whitebar for example'
  },
  {
    property: 'onClick',
    propType: 'Function',
    description: 'handle click event"'
  }
]

storiesOf('Icons', module).add('date', () => <Icon path="/date.svg" />, {
  notes: 'You can change the icon path in ./components/Icon.js',
  info: {
    inline: true,
    TableComponent: () => <TableComponent propDefinitions={propDefinitions} />
  }
})
