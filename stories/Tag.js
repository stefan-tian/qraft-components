import React from 'react'
import { storiesOf } from '@storybook/react'
import { Tag } from '../src/components'
import TableComponent from '../examples/TableComponent'

const propDefinitions = [
  {
    property: 'mb',
    propType: 'String',
    description: 'Customize margin bottom. e.g. "30px"'
  },
  {
    property: 'mr',
    propType: 'String',
    description: 'Customize margin right. e.g. "15px"'
  },
  {
    property: 'ongoing',
    propType: 'Boolean',
    description:
      'When specified(or set to true), color -> white, background -> #ff730e'
  },
  {
    property: 'finished',
    propType: 'Boolean',
    description:
      'When specified(or set to true), color -> #9b9b9b, background -> #ececec'
  }
]

storiesOf('Tags', module)
  .addParameters({
    info: {
      inline: true,
      TableComponent: () => <TableComponent propDefinitions={propDefinitions} />
    }
  })
  .add('in-progress', () => <Tag ongoing text="進行中" />, {
    notes:
      'component: Tag\nprops: ongoing, finished, mr/mb: margin-right/bottom\nThis one is progress'
  })
  .add('finished', () => <Tag finished text="已完成" />, {
    notes:
      'component: Tag\nprops: ongoing, finished, mr/mb: margin-right/bottom\nThis one is finished'
  })
