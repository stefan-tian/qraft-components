import React from 'react'
import { storiesOf } from '@storybook/react'
import { Text } from '../src/components'
import TableComponent from '../examples/TableComponent'

Text.displayName = 'Text'

const propDefinitions = [
  {
    property: 'mb',
    propType: 'String',
    description: 'Customize margin bottom. e.g. "30px"'
  },
  {
    property: 'mr',
    propType: 'String',
    description: 'Customize margin right. e.g. "15px"'
  },
  {
    property: 'fs',
    propType: 'String',
    description: 'Customize the font size. e.g. "16px"'
  },
  {
    property: 'ls',
    propType: 'String',
    description: "Customize the letter-spacing e.g. '1.77px'"
  },
  {
    property: 'lh',
    propType: 'String',
    description: "Customize the line-height e.g. '17px'"
  },
  {
    property: 'justify',
    propType: 'Boolean',
    description: 'Set text-align to justify'
  },
  {
    property: 'center',
    propType: 'Boolean',
    description: 'Set text-align to center'
  },
  {
    property: 'regular',
    propType: 'Boolean',
    description: 'Set font-weight to normal'
  },
  {
    property: 'bold',
    propType: 'Boolean',
    description: 'Set font-weight to bold'
  },
  {
    property: 'main',
    propType: 'Boolean',
    description: 'Set color to #ff730e'
  },
  {
    property: 'white',
    propType: 'Boolean',
    description: 'Set color to white'
  },
  {
    property: 'gray',
    propType: 'Boolean',
    description: 'Set color to #9b9b9b'
  },
  {
    property: 'grayLight',
    propType: 'Boolean',
    description: 'Set color to #cdcdcd'
  },
  {
    property: 'alert',
    propType: 'Boolean',
    description: 'Set color to #ed4b34'
  },
  {
    property: 'grayDark',
    propType: 'Boolean',
    description: 'Set color to #7b7b7b'
  },
  {
    property: 'xs',
    propType: 'Boolean',
    description: 'Set font-size: 12px; letter-spacing: 0.58px'
  },
  {
    property: 'sm',
    propType: 'Boolean',
    description:
      'Set font-size: 14px; letter-spacing: 0.67px; line-height: 18px'
  },
  {
    property: 'md',
    propType: 'Boolean',
    description:
      'Set font-size: 16px; letter-spacing: 0.77px; line-height: 24px'
  },
  {
    property: 'lg',
    propType: 'Boolean',
    description: 'Set font-size: 18px; letter-spacing: 0.86px'
  },
  {
    property: 'xl',
    propType: 'Boolean',
    description:
      'Set font-size: 20px; letter-spacing: 0.77px; line-height: 26px; font-weight: bold'
  }
]

storiesOf('TextStyles', module).add(
  'different sizes/styles',
  () => (
    <div>
      <Text bold fs="32px" ls="1.54px" lh="30px" mb="5px">
        Topic
      </Text>
      <Text bold fs="28px" ls="1.34px" lh="34px" mb="5px">
        Topic-Small
      </Text>
      <Text bold fs="24px" ls="1.15px" lh="32px" mb="5px">
        Highlight-light
      </Text>
      <Text bold main fs="24px" ls="1.15px" lh="32px" mb="5px">
        Highlight-main
      </Text>
      <Text xl mb="5px">
        Title
      </Text>
      <Text lg bold mb="5px">
        Subtitle-Bold
      </Text>
      <Text lg mb="5px">
        Subtitle
      </Text>
      <Text md bold mb="5px">
        Content-bold
      </Text>
      <Text grayLight md mb="5px">
        Content-gray-light
      </Text>
      <Text main md mb="5px">
        Content-main
      </Text>
      <Text sm mb="5px">
        Description
      </Text>
      <Text main sm mb="5px">
        Description-main
      </Text>
      <Text xs>Label</Text>
      <Text xs gray>
        Label-gray
      </Text>
      <Text xs alert>
        Error
      </Text>
    </div>
  ),
  {
    notes:
      'component: Text\nprops:\n [color]: main, gray, grayLight, grayDark, alert, white,\n [size]: xs: 12px, sm: 14px, md: 16px, lg: 18px, xl: 20px, \n[weight]: bold, regular, default is medium, \n[custom]: lh: line-height, ls: letter-spacing, mb: margin-bottom, mr: margin-right, fs: font-size',
    info: {
      inline: true,
      TableComponent: () => <TableComponent propDefinitions={propDefinitions} />
    }
  }
)
