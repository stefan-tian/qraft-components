import React from 'react'
import { storiesOf } from '@storybook/react'
import {
  DropDownContainer,
  DropDownMobileInfo,
  DropDownItem,
  DropDownSepline,
  DropDownUserInfo
} from '../src/components'
import TableComponent from '../examples/TableComponent'

const propDefinitions = [
  {
    property: 'name',
    propType: 'String',
    description: 'The name of the user'
  },
  {
    property: 'avatarUrl',
    propType: 'String',
    description: 'The url(src) of the avatar'
  }
]

DropDownContainer.displayName = 'DropDownContainer'
DropDownMobileInfo.displayName = 'DropDownMobileInfo'
DropDownItem.displayName = 'DropDownItem'
DropDownSepline.displayName = 'DropDownSepline'

storiesOf('DropDown', module)
  .add('DropDownUserInfo', () => <DropDownUserInfo name="Q匠" />, {
    info: {
      inline: true,
      TableComponent: () => <TableComponent propDefinitions={propDefinitions} />
    }
  })
  .add('DropDownItem', () => <DropDownItem text="個人檔案" />, {
    info: {
      inline: true
    },
    notes: 'See Menu.Item on blueprint for more details'
  })
  .add(
    'DropDownDemo',
    () => (
      <DropDownContainer>
        <DropDownMobileInfo>
          <DropDownUserInfo name="蘇婷" />
          <DropDownItem>我的活動</DropDownItem>
          <DropDownSepline />
        </DropDownMobileInfo>
        <DropDownItem>個人檔案</DropDownItem>
        <DropDownItem>帳號設定</DropDownItem>
        <DropDownItem>訂購紀錄</DropDownItem>
        <DropDownItem>登出</DropDownItem>
      </DropDownContainer>
    ),
    {
      info: {
        inline: true
      }
    }
  )
