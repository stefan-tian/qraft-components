import React from 'react'
import { storiesOf } from '@storybook/react'
import {
  Footer,
  FooterContainer,
  LeftFooter,
  RightFooter,
  Text,
  IconBtn,
  Button
} from '../src/components'

Footer.displayName = 'Footer'
FooterContainer.displayName = 'FooterContainer'
LeftFooter.displayName = 'LeftFooter'
RightFooter.displayName = 'RightFooter'

storiesOf('Footer', module).add(
  'footer',
  () => (
    <Footer>
      <FooterContainer>
        <LeftFooter>
          <Text main md>
            NT$
          </Text>
          <Text main fs="24px" lh="36px" mr="8px">
            149
          </Text>
          <Text md gray mr="12px">
            11/12前早鳥
          </Text>
          <Text md gray>
            原價
          </Text>
          <Text md gray style={{ textDecoration: 'line-through' }}>
            NT$299
          </Text>
        </LeftFooter>
        <RightFooter>
          <Button stretchy main text="立即參與" mr="12px" />
          <IconBtn text="試聽" path="/icon-headset.svg" iconWidth="18px" />
        </RightFooter>
      </FooterContainer>
    </Footer>
  ),
  {
    info: {
      inline: true
    }
  }
)
