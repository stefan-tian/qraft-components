import React from 'react'
import { storiesOf } from '@storybook/react'
import { Popover } from '../src/components'
import TableComponent from '../examples/TableComponent'

const propDefinitions = [
  {
    property: 'mr',
    propType: 'String',
    description: 'customize margin right. e.g. "5px"'
  },
  {
    property: 'mb',
    propType: 'String',
    description: 'customize margin bottom. e.g. "5px"'
  }
]

storiesOf('Popover', module).add('popover', () => <Popover />, {
  info: {
    inline: true,
    TableComponent: () => <TableComponent propDefinitions={propDefinitions} />
  }
})
