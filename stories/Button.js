import React from 'react'
import { storiesOf } from '@storybook/react'
import { Button } from '../src/components'
import TableComponent from '../examples/TableComponent'

const propDefinitions = [
  {
    property: 'primary',
    propType: 'Boolean',
    description: 'style the button like the primary one in the style guide'
  },
  {
    property: 'main',
    propType: 'Boolean',
    description: 'style the button like the main one in the style guide'
  },
  {
    property: 'fs',
    propType: 'String',
    description: 'customize the font size. e.g. "16px"'
  },
  {
    property: 'mr',
    propType: 'String',
    description: 'customize margin right. e.g. "5px"'
  },
  {
    property: 'mb',
    propType: 'String',
    description: 'customize margin bottom. e.g. "5px"'
  },
  {
    property: 'stretchy',
    propType: 'Boolean',
    description:
      'stretch to full width when max-width is 576px, see footer component for example"'
  },
  {
    property: 'onClick',
    propType: 'Function',
    description: 'handle click event"'
  }
]

storiesOf('Buttons', module)
  .addParameters({
    info: {
      inline: true,
      TableComponent: () => <TableComponent propDefinitions={propDefinitions} />
    }
  })
  .add('primary', () => <Button primary text="查看更多" />, {
    notes:
      'component: Button\nprops: primary, main, fs: font-size, mr/mb: margin-right/bottom\n This one is primary'
  })
  .add('main', () => <Button main text="立即登入" />, {
    notes: 'component: Button\nThis one is main'
  })
